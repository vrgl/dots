;; Clean interface

(menu-bar-mode 0)
(tool-bar-mode 0)
(scroll-bar-mode 0)
(global-display-line-numbers-mode 1)

;; Do not create backup files

(setq make-backup-files nil)
(setq auto-save-default nil)

;; Modalka config ;;

(modalka-define-kbd "h" "C-b")
(modalka-define-kbd "j" "C-n")
(modalka-define-kbd "k" "C-p")
(modalka-define-kbd "l" "C-f")

(modalka-define-kbd "J" "C-v")
(modalka-define-kbd "K" "M-v")

(modalka-define-kbd "y" "M-w")
(modalka-define-kbd "w" "C-w")
(modalka-define-kbd "p" "C-y")

(modalka-define-kbd "a" "C-a")
(modalka-define-kbd "e" "C-e")
(modalka-define-kbd "b" "M-b")
(modalka-define-kbd "f" "M-f")

(modalka-define-kbd "v" "C-SPC")
(modalka-define-kbd "cv" "C-x SPC")

(modalka-define-kbd "o" "C-o")
(modalka-define-kbd "u" "C-x u")

(modalka-define-kbd "q" "C-d")
(modalka-define-kbd "de" "C-k")

(modalka-define-kbd "xs" "C-x C-s")
(modalka-define-kbd "xf" "C-x C-f")

(modalka-define-kbd "xb" "C-x b")

(modalka-define-kbd "M-<tab>" "C-x o")
(modalka-define-kbd "x1" "C-x 1")
(modalka-define-kbd "x2" "C-x 2")
(modalka-define-kbd "x3" "C-x 3")
(modalka-define-kbd "xk" "C-x k")

(define-key modalka-mode-map (kbd "i") 'modalka-mode)
(global-set-key (kbd "<escape>") 'modalka-mode)
(define-key modalka-mode-map (kbd "<escape>") 'keyboard-escape-quit)

(setq-default cursor-type '(bar . 1))
(setq modalka-cursor-type 'box)

(modalka-global-mode 1)

;; Customize stuff

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes
   '("4c56af497ddf0e30f65a7232a8ee21b3d62a8c332c6b268c81e9ea99b11da0d3" default)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit nil :extend nil :stipple nil :inverse-video nil :box nil :strike-through nil :overline nil :underline nil :slant normal :weight normal :height 130 :width normal :foundry "ADBO" :family "CaskaydiaCove NFM")))))
