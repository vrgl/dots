# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias dzat='devour zathura'
PS1='[\u@\h \W]\$ '

export PATH=$PATH:$HOME/.local/bin
complete -cf doas
complete -cf loginctl

alias dv='devour'
complete -cf dv

alias dz='devour zathura'

alias pcp='cat /sys/class/power_supply/BAT1/capacity'
export PS1="┌─[\[\e[01;32m\]\u\[\e[00m\]@\[\e[01;32m\]\h\[\e[00m\]:\[\e[1;34m\]\w\[\e[0m\]]\n└─╼ "

#if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
#  exec startx
#fi
